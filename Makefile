# Makefile for text2048

CC = gcc
CFLAGS = -std=c99 -Wall -pedantic -O3 -s 
LIBS = -lncursesw
OBJS = text2048.o map_ops.o
MAIN = text2048

all: $(MAIN)

$(MAIN): $(OBJS) 
	$(CC) -o $(MAIN) $(CFLAGS) $(OBJS) $(LIBS) 

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	$(RM) *.o $(MAIN)

.PHONY: all clean

