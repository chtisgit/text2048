#ifndef __TEXT2048_H__
#define __TEXT2048_H__

#define GAME_COLS	4

struct GameMap{
	int field[GAME_COLS][GAME_COLS];
};

void swap_int(int *const, int *const);

#endif

