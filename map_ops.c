
#include "text2048.h"

#if 0
void extract_map(unsigned int array[GAME_COLS][GAME_COLS]){
	int i;

	for(i = 0; i < GAME_COLS; i++){
		array[0][i] = map->field[i] & 0xFF;
		array[1][i] = (map->field[i] >> 8) & 0xFF;
		array[2][i] = (map->field[i] >> 16) & 0xFF;
		array[3][i] = (map->field[i] >> 24) & 0xFF;
	}
}

void compress_map(const unsigned int array[GAME_COLS][GAME_COLS]){
	int i;

	for(i = 0; i < GAME_COLS; i++){
		map->field[i] = array[0][i] | (array[1][i] << 8) | 
			(array[2][i] << 16) | (array[3][i] << 24);
	}
}
#endif

int map_check_move_possible(const struct GameMap *const map)
{
	int x,y;
	for(y = 0; y < GAME_COLS; y++){
		for(x = 0; x < GAME_COLS-1; x++){
			if(map->field[x][y] == map->field[x+1][y] || map->field[x][y] == 0)
				return 1;
		}
		if(map->field[GAME_COLS-1][y] == 0)
			return 1;
	}
	for(x = 0; x < GAME_COLS; x++){
		for(y = 0; y < GAME_COLS-1; y++){
			if(map->field[x][y] == map->field[x][y+1] || map->field[x][y] == 0)
				return 1;
		}
		if(map->field[x][GAME_COLS-1] == 0)
			return 1;
	}
	return 0;
}

int toleft(struct GameMap *const map, int line){
	int i = 0, j = 0, actions = 0;
	for(i = 0; i < GAME_COLS; i++){
		if(map->field[i][line] != 0){
			if(i != j){
				map->field[j][line] = map->field[i][line];
				map->field[i][line] = 0;
				actions++;
			}
			j++;
		}
	}
	return actions;
}

int map_leftop(struct GameMap *const map){
	int i,line;
	int actions = 0;

	for(line = 0; line < GAME_COLS; line++){
		actions += toleft(map, line);
		for(i = 0; i < GAME_COLS-1; i++){
			if(map->field[i][line] != 0 && map->field[i][line] == map->field[i+1][line]){
				map->field[i][line]++;
				map->field[i+1][line] = 0;
				actions++;
			}
		}
		actions += toleft(map, line);
	}

	return actions;
}

void map_transpose(struct GameMap *const map){
	int x,y;
	for(y = 0; y < GAME_COLS; y++){
		for(x = y+1; x < GAME_COLS; x++){
			swap_int(&map->field[x][y], &map->field[y][x]);
		}
	}
}

void map_rotate_left(struct GameMap *const map, int times){
	int x,y;

	times %= 4;
	if(times == 0) return;

	map_transpose(map);
	for(x = 0; x < GAME_COLS; x++){
		for(y = 0; y < GAME_COLS/2; y++){
			swap_int(&map->field[x][y], &map->field[x][GAME_COLS-y-1]);
		}
	}

	map_rotate_left(map, times-1);	
}

int map_upop(struct GameMap *const map){
	map_rotate_left(map, 1);
	int actions = map_leftop(map);
	map_rotate_left(map, 3);
	return actions;
}

int map_downop(struct GameMap *const map){
	map_rotate_left(map, 3);
	int actions = map_leftop(map);
	map_rotate_left(map, 1);
	return actions;
}

int map_rightop(struct GameMap *const map){
	map_rotate_left(map, 2);
	int actions = map_leftop(map);
	map_rotate_left(map, 2);
	return actions;
}
