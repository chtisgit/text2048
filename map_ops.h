#ifndef __MAP_OPS_H__
#define __MAP_OPS_H__


int map_leftop(struct GameMap *const map);
int map_rightop(struct GameMap *const map);
int map_upop(struct GameMap *const map);
int map_downop(struct GameMap *const map);
int map_check_move_possible(struct GameMap *const map);


#endif

