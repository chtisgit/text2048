![project logo](https://github.com/chtisgit/text2048/blob/master/logo.png)

This is an ncurses version of the game "2048"

text2048 is a light-weight version of the browser and smartphone game 2048.

It uses ncurses to display the game in the console.

