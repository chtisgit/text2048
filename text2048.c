#include <ctype.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <curses.h>

#include "text2048.h"
#include "map_ops.h"

void swap_int(int *const a, int *const b){
	if(a != b){
		register int tmp = *a;
		*a = *b;
		*b = tmp;
	}
}


inline int pow2(int exp)
{
	int x = 1;
	if(exp < 0) return 0;
	while(exp--)
		x *= 2;
	return x;
}

void drawmap(const struct GameMap *const map)
{
	int x,y;
	char str[50];

	clear();

	for(y = 0; y < GAME_COLS; y++){
		for(x = 0; x < GAME_COLS; x++){
			const int text_y = 2*y+2, text_x = 8*x+2;

			if(map->field[x][y] == 0){
				color_set(2+map->field[x][y]/2, 0);
				mvaddstr(text_y, text_x, "       ");
				
			}else{
				if(map->field[x][y] % 2 == 1){
					color_set(1, 0);
					mvaddstr(text_y, text_x, "       ");
					sprintf(str, "%5d", pow2(map->field[x][y]));
					color_set(2+map->field[x][y]/2, 0);
					mvaddstr(text_y, text_x+1, str);
				}else{
					sprintf(str, " %5d ", pow2(map->field[x][y]));
					color_set(2+map->field[x][y]/2, 0);
					mvaddstr(text_y, text_x, str);
				}
			}
		}
	}
}

void shuffle(int vec[], const int len)
{
	int i,pos1,pos2;
	for(i = 0; i < 2*len; i++){
		pos1 = rand() % GAME_COLS;
		pos2 = rand() % GAME_COLS;
		
		swap_int(&vec[pos1], &vec[pos2]);
	}

}

int spawn_number(struct GameMap *const map)
{
	int line_vec[GAME_COLS], col_vec[GAME_COLS];
	int i,j;
	
	for(i = 0; i < GAME_COLS; i++)
		line_vec[i] = i;
	for(i = 0; i < GAME_COLS; i++)
		col_vec[i] = i;

	shuffle(col_vec,GAME_COLS);
	shuffle(line_vec,GAME_COLS);

	for(j = 0; j < GAME_COLS; j++){
		for(i = 0; i < GAME_COLS; i++){
			const int x = col_vec[i], y = line_vec[j];
			
			if(map->field[x][y] == 0){
				int w = (rand() % 40)/4;
				if(w == 0)
					map->field[x][y] = 2;
				else
					map->field[x][y] = 1;

				if(!map_check_move_possible(map))
					return 0;
				
				return 1;
			}
		}
	}

	return 0;
}

int check_2048(const struct GameMap *const map)
{
	int x,y;
	int r = 0;
	for(y = 0; y < GAME_COLS; y++)
		for(x = 0; x < GAME_COLS; x++)
			r |= map->field[x][y] == 11 ? 1 : 0;
	return r;
}

int game(void)
{
	struct GameMap map;
	int got_2048 = 0;

	memset((void*)&map, 0, sizeof(map));
	
	spawn_number(&map);
	spawn_number(&map);

	int key;
	for(;;){
		drawmap(&map);

		color_set(10, 0);
		mvaddstr(LINES-2,2,"Use arrow keys to play "
				"/ Press Q to exit");
		if(got_2048)
			mvaddstr(LINES-3,2,"You won! You got 2048, but feel free to continue playing");


		refresh();

		key = getch();
		if(tolower(key) == 'q') exit(0);
		
		switch(key){
		case KEY_LEFT:
			if(map_leftop(&map)){
				if(!got_2048)
					got_2048 |= check_2048(&map);
				if(!spawn_number(&map))
					goto exit_repaint;
			}
			break;
		case KEY_RIGHT:
			if(map_rightop(&map)){
				if(!got_2048)
					got_2048 |= check_2048(&map);
				if(!spawn_number(&map))
					goto exit_repaint;
			}
			break;
		case KEY_UP:
			if(map_upop(&map)){
				if(!got_2048)
					got_2048 |= check_2048(&map);
				if(!spawn_number(&map))
					goto exit_repaint;
			}
			break;
		case KEY_DOWN:
			if(map_downop(&map)){
				if(!got_2048)
					got_2048 |= check_2048(&map);
				if(!spawn_number(&map))
					goto exit_repaint;
			}
			break;
		}
	}

	/* we need this because we can't access map
	   after we left this function */
exit_repaint:
	drawmap(&map);
	return got_2048;
}

int main(int argc, char **argv)
{
	initscr();
	atexit((void (*)(void)) endwin);
	start_color();
	init_pair(1, COLOR_WHITE, COLOR_RED); 
	init_pair(2, COLOR_WHITE, COLOR_YELLOW); 
	init_pair(3, COLOR_WHITE, COLOR_GREEN); 
	init_pair(4, COLOR_WHITE, COLOR_BLUE); 
	init_pair(5, COLOR_WHITE, COLOR_MAGENTA);
	init_pair(6, COLOR_YELLOW, COLOR_CYAN);
	init_pair(7, COLOR_YELLOW, COLOR_BLACK);
	init_pair(8, COLOR_GREEN, COLOR_BLACK);
	init_pair(9, COLOR_RED, COLOR_BLACK);
	init_pair(10, COLOR_WHITE, COLOR_BLACK);
	attron(A_BOLD);
	curs_set(0);
	cbreak();
	keypad(stdscr, TRUE);

	int c;
	do{
		int won = game();
		
		if(won)
			color_set(8,0);
		else
			color_set(9, 0);

		mvaddstr(LINES-4,2,"GAME OVER");
		mvaddstr(LINES-3,2,"Press Q to exit");
		mvaddstr(LINES-2,2,"Press any other key to play again");

		refresh();
	
		do
			c = getch();
		while(c == KEY_LEFT || c == KEY_RIGHT || c == KEY_UP || c == KEY_DOWN);
		/* ignoring arrow keys will prevent accidental new game */

	}while(c != 'q' && c != 'Q');

	return 0;
}
